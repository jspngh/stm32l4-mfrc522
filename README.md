# Example project for mfrc522 + embassy
This can be used as a template if you want to use the [mfrc522](https://gitlab.com/jspngh/rfid-rs) rust library
together with [embassy](https://embassy.dev/) on an STM32L4.

If you target another MCU, you'll have to make the necessary changes,
but it should be very similar.

The code was tested with the `1.70.0-nightly` toolchain.
